# Atlassian Versions

Find the latest version of these Atlassian products.

Add your own!

Track your favorite Add On!

Integrate version tracking into your IaC pipeline!

![](versions.gif)
## Requirements

- curl
- jq

Install curl and jq as required by your system.

Clone or download the version.sh script.

Run it.

Add products as needed.

## References

https://developer.atlassian.com/platform/marketplace/rest/#api-applications-applicationKey-versions-latest-get

## See Also

https://www.linkedin.com/pulse/i-have-insomnia-like-william-kennedy/